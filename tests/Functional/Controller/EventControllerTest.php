<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Tests\Functional\BaseFunctionalTest;
use DateTime;
use Firebase\JWT\JWT;

abstract class EventControllerTest extends BaseFunctionalTest
{
    protected function getJwt(string $userEmail): string
    {
        return JWT::encode(
            [
                'user' => $userEmail,
                'exp'  => (new DateTime())->modify('+10 minutes')->getTimestamp(),
            ],
            self::$container->getParameter('jwt_secret'),
            self::$container->getParameter('jwt_algo')
        );
    }
}