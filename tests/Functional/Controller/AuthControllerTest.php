<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Tests\DataFixtures\UserFixtures;
use App\Tests\Functional\BaseFunctionalTest;
use GuzzleHttp\Utils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class AuthControllerTest extends BaseFunctionalTest
{
    /**
     * @dataProvider registerDataProvider
     */
    public function testRegister(array $testUserData, int $httpResponse, array $decodedResponse): void
    {
        $this->request('/auth/register', Request::METHOD_POST, $testUserData);
        self::assertSame($httpResponse, $this->client->getResponse()->getStatusCode());
        self::assertSame($decodedResponse, Utils::jsonDecode($this->client->getResponse()->getContent(), true));
    }

    public static function registerDataProvider(): array
    {
        return [
            'normal case' => [
                [
                    'email' => 'test00@example.com',
                    'password' => '1232153',
                ],
                Response::HTTP_CREATED,
                ['message' => 'test00@example.com']
            ],
            'missed elem case' => [
                [
                    'email' => 'test00@example.com',
                ],
                Response::HTTP_BAD_REQUEST,
                ['error' => 'Invalid property password: The property password is required']
            ],
            'invalid email' => [
                [
                    'email' => 'test00com',
                    'password' => '1232153',
                ],
                Response::HTTP_BAD_REQUEST,
                ['error' => 'Invalid property email: Invalid email']
            ],
            'duplication case' => [
                [
                    'email' => UserFixtures::STORED_TEST_USER_EMAIL,
                    'password' => 'fasfa',
                ],
                Response::HTTP_BAD_REQUEST,
                ['error' => 'User already exists']
            ],
        ];
    }

    /**
     * @dataProvider loginDataProvider
     */
    public function testLogin(array $testUserData, int $httpResponse, string $messageKey, ?string $errorMessage): void
    {
        $this->request('/auth/login', Request::METHOD_POST, $testUserData);
        self::assertSame($httpResponse, $this->client->getResponse()->getStatusCode());
        $response = Utils::jsonDecode($this->client->getResponse()->getContent(), true);
        self::assertArrayHasKey($messageKey, $response);
        if (isset($response['error'])) {
            self::assertSame($errorMessage, $response['error']);
        }
    }

    public static function loginDataProvider(): array
    {
        return [
            'normal case' => [
                [
                    'email' => UserFixtures::STORED_TEST_USER_EMAIL,
                    'password' => UserFixtures::STORED_TEST_USER_PASS,
                ],
                Response::HTTP_OK,
                'message',
                null
            ],
            'wrong email' => [
                [
                    'email' => 'fixtest2@example.com',
                    'password' => UserFixtures::STORED_TEST_USER_PASS,
                ],
                Response::HTTP_BAD_REQUEST,
                'error',
                'Email or password is wrong',
            ],
            'wrong pass' => [
                [
                    'email' => UserFixtures::STORED_TEST_USER_EMAIL,
                    'password' => '????',
                ],
                Response::HTTP_BAD_REQUEST,
                'error',
                'Email or password is wrong',
            ],
            'missed elem case' => [
                [
                    'email' => UserFixtures::STORED_TEST_USER_EMAIL,
                ],
                Response::HTTP_BAD_REQUEST,
                'error',
                'Invalid property password: The property password is required',
            ],
        ];
    }
}