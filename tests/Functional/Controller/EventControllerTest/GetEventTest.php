<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller\EventControllerTest;

use App\Tests\DataFixtures\EventFixtures;
use App\Tests\Functional\Controller\EventControllerTest;
use GuzzleHttp\Utils;
use Symfony\Component\HttpFoundation\Request;

final class GetEventTest extends EventControllerTest
{
    /**
     * @dataProvider getEventDataProvider
     */
    public function testGetEvent(string $eventId, string $userEmail, array $responseBody): void
    {
        $jwt = $this->getJwt($userEmail);
        $this->request("/api/event/$eventId", Request::METHOD_GET, [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $controllerResponse = Utils::jsonDecode($this->client->getResponse()->getContent(), true);
        self::assertSame($responseBody, $controllerResponse);
    }

    public static function getEventDataProvider(): array
    {
        return [
            'normal case' => [
                EventFixtures::EVENT_1_UUID,
                EventFixtures::EVENT_TEST_USER_EMAIL,
                [
                    'message' => [
                        'uuid' => EventFixtures::EVENT_1_UUID,
                        'name' => 'Very important event',
                        'venue' => 'Earth',
                        'coordinate' => [
                            'latitude' => 10.0001,
                            'longitude' => -120.5664,
                        ],
                        'startDate' => '2020-12-01 12:00:00',
                        'endDate' => '2020-12-01 13:00:00',
                    ]
                ],
            ],
            'normal lightweight case' => [
                EventFixtures::EVENT_2_UUID,
                EventFixtures::EVENT_TEST_USER_EMAIL,
                [
                    'message' => [
                        'uuid' => EventFixtures::EVENT_2_UUID,
                        'name' => null,
                        'venue' => null,
                        'coordinate' => null,
                        'startDate' => null,
                        'endDate' => null,
                    ]
                ],
            ],
            'can be found by another user' => [
                EventFixtures::EVENT_1_UUID,
                EventFixtures::EVENT_TEST_USER_EMAIL,
                [
                    'message' => [
                        'uuid' => EventFixtures::EVENT_1_UUID,
                        'name' => 'Very important event',
                        'venue' => 'Earth',
                        'coordinate' => [
                            'latitude' => 10.0001,
                            'longitude' => -120.5664,
                        ],
                        'startDate' => '2020-12-01 12:00:00',
                        'endDate' => '2020-12-01 13:00:00',
                    ]
                ],
            ],
            'not found event' => [
                '1c40cfba-1992-4eca-a178-5cfd08b56027',
                EventFixtures::EVENT_TEST_USER_EMAIL,
                [
                    'error' => 'Event 1c40cfba-1992-4eca-a178-5cfd08b56027 was not found',
                ],
            ],
        ];
    }
}