<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller\EventControllerTest;

use App\Tests\DataFixtures\UserFixtures;
use App\Tests\Functional\Controller\EventControllerTest;
use GuzzleHttp\Utils;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CreateEventTest extends EventControllerTest
{
    /**
     * @dataProvider createEventDataProvider
     */
    public function testCreateEvent(array $testUserData, int $httpResponse, string $userEmail, ?array $decodedResponseIfNotOk): void
    {
        $jwt = $this->getJwt($userEmail);
        $this->request('/api/event', Request::METHOD_POST, $testUserData, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $controllerResponse = Utils::jsonDecode($this->client->getResponse()->getContent(), true);
        $controllerResponseCode = $this->client->getResponse()->getStatusCode();

        if ($controllerResponseCode === Response::HTTP_CREATED) {
            self::assertTrue(Uuid::isValid($controllerResponse['message']));
        } else {
            self::assertSame($decodedResponseIfNotOk, $controllerResponse);
        }
        self::assertSame($httpResponse, $this->client->getResponse()->getStatusCode());
    }

    public static function createEventDataProvider(): array
    {
        return [
            'normal case' => [
                [
                    'name' => 'Some fancy event name',
                    'venue' => 'Assume it is a string name of a venue',
                    'coordinate' => [
                        'latitude' => 89.123456,
                        'longitude' => 179.2,
                    ],
                    'startDate' => '2020-12-12T20:20:39+00:00',
                    'endDate' => '2020-12-12T20:21:39+00:00'
                ],
                Response::HTTP_CREATED,
                UserFixtures::STORED_TEST_USER_EMAIL,
                null,
            ],
            'correct lightweight case' => [
                [],
                Response::HTTP_CREATED,
                UserFixtures::STORED_TEST_USER_EMAIL,
                null,
            ],
            'correct non-positive coordinate' => [
                [
                    'name' => 'Some fancy event name',
                    'venue' => 'Assume it is a string name of a venue',
                    'coordinate' => [
                        'latitude' => 0,
                        'longitude' => -178.2,
                    ],
                    'startDate' => '2020-12-12T20:20:39+00:00',
                    'endDate' => '2020-12-12T20:21:39+00:00'
                ],
                Response::HTTP_CREATED,
                UserFixtures::STORED_TEST_USER_EMAIL,
                null,
            ],
            'missed coordinate' => [
                [
                    'name' => 'Some fancy event name',
                    'venue' => 'Assume it is a string name of a venue',
                    'coordinate' => [
                        'longitude' => 179.2,
                    ],
                    'startDate' => '2020-12-12T20:20:39+00:00',
                    'endDate' => '2020-12-12T20:21:39+00:00'
                ],
                Response::HTTP_BAD_REQUEST,
                UserFixtures::STORED_TEST_USER_EMAIL,
                ['error' => 'Invalid property coordinate.latitude: The property latitude is required'],
            ],
            'incorrect coordinate: out of range' => [
                [
                    'name' => 'Some fancy event name',
                    'venue' => 'Assume it is a string name of a venue',
                    'coordinate' => [
                        'latitude' => 89.123456,
                        'longitude' => 181.2,
                    ],
                    'startDate' => '2020-12-12T20:20:39+00:00',
                    'endDate' => '2020-12-12T20:21:39+00:00'
                ],
                Response::HTTP_BAD_REQUEST,
                UserFixtures::STORED_TEST_USER_EMAIL,
                ['error' => 'Invalid property coordinate.longitude: Must have a maximum value of 180'],
            ],
            'user missed in DB but JWT is correct case' => [
                [
                    'name' => 'Some fancy event name',
                    'venue' => 'Assume it is a string name of a venue',
                    'coordinate' => [
                        'latitude' => 89.123456,
                        'longitude' => 179.123456,
                    ],
                    'startDate' => '2020-12-12T20:20:39+00:00',
                    'endDate' => '2020-12-12T20:21:39+00:00'
                ],
                Response::HTTP_UNAUTHORIZED,
                'hi@example.com',
                ['message' => 'Authentication Required'],
            ]
        ];
    }
}