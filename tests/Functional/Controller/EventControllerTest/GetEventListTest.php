<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller\EventControllerTest;

use App\Tests\DataFixtures\EventFixtures;
use App\Tests\Functional\Controller\EventControllerTest;
use GuzzleHttp\Utils;
use Symfony\Component\HttpFoundation\Request;

final class GetEventListTest extends EventControllerTest
{
    /**
     * @dataProvider getEventListDataProvider
     */
    public function testGetEventList(array $query, string $userEmail, int $responseEventsAmount): void
    {
        $jwt = $this->getJwt($userEmail);
        $this->request("/api/eventList", Request::METHOD_POST, $query, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $controllerResponse = Utils::jsonDecode($this->client->getResponse()->getContent(), true);
        self::assertCount($responseEventsAmount, $controllerResponse['events']);
    }

    public static function getEventListDataProvider(): array
    {
        return [
            'search by name' => [
                [
                    'name' => 'Very important event'
                ],
                EventFixtures::EVENT_TEST_USER_EMAIL,
                1,
            ],

            'search by name and venue' => [
                [
                    'name' => 'Another important event',
                    'venue' => 'Earth',
                ],
                EventFixtures::EVENT_TEST_USER_EMAIL,
                2
            ],

            'search by nothing' => [
                [],
                EventFixtures::EVENT_TEST_USER_EMAIL,
                0
            ],

            'search by coordinates' => [
                [
                    'coordinate' => [
                        'latitude' => 10.6513,
                        'longitude' => -100.5664,
                    ]
                ],
                EventFixtures::EVENT_TEST_USER_EMAIL,
                2
            ]
        ];
    }


}