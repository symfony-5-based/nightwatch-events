<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller\EventControllerTest;

use App\Entity\Event;
use App\Tests\DataFixtures\EventFixtures;
use App\Tests\DataFixtures\UserFixtures;
use App\Tests\Functional\Controller\EventControllerTest;
use GuzzleHttp\Utils;
use Symfony\Bridge\PhpUnit\ClockMock;
use Symfony\Component\HttpFoundation\Request;

final class DeleteEventTest extends EventControllerTest
{
    /**
     * @dataProvider deleteEventDataProvider
     */
    public function testDeleteEvent(string $currentTime, string $eventUuid, string $userEmail, array $responseBody): void
    {
        ClockMock::register(Event::class);
        ClockMock::withClockMock(date_create($currentTime)->getTimestamp());
        $jwt = $this->getJwt($userEmail);
        $this->request("/api/event/$eventUuid", Request::METHOD_DELETE, [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $controllerResponse = Utils::jsonDecode($this->client->getResponse()->getContent(), true);
        self::assertSame($responseBody, $controllerResponse);
    }

    public static function deleteEventDataProvider(): array
    {
        return [
            'normal case' => [
                '2020-12-01T11:00:00+00:00',
                EventFixtures::EVENT_1_UUID,
                EventFixtures::EVENT_TEST_USER_EMAIL,
                ['message' => 'Event was deleted'],
            ],
            'empty datetime case' => [
                '2020-12-01T11:00:00+00:00',
                EventFixtures::EVENT_2_UUID,
                EventFixtures::EVENT_TEST_USER_EMAIL,
                ['message' => 'Event was deleted'],
            ],
            'too late case' => [
                '2020-12-01T12:30:00+00:00',
                EventFixtures::EVENT_1_UUID,
                EventFixtures::EVENT_TEST_USER_EMAIL,
                ['error' => 'Ongoing event cannot be deleted'],
            ],
            'after event is finished' => [
                '2020-12-01T13:00:01+00:00',
                EventFixtures::EVENT_1_UUID,
                EventFixtures::EVENT_TEST_USER_EMAIL,
                ['message' => 'Event was deleted'],
            ],
            'not found case' => [
                '2020-12-01T11:00:00+00:00',
                '51188ffd-d305-4a7f-8ed3-17dfee08b8df',
                EventFixtures::EVENT_TEST_USER_EMAIL,
                ['error' => 'Event 51188ffd-d305-4a7f-8ed3-17dfee08b8df was not found'],
            ],
            'someone\'s event' => [
                '2020-12-01T11:00:00+00:00',
                EventFixtures::EVENT_3_UUID,
                UserFixtures::STORED_TEST_USER_EMAIL,
                ['error' => 'User has no rights to delete the event'],
            ],
        ];
    }
}