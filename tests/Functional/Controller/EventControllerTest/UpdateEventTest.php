<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller\EventControllerTest;

use App\Tests\DataFixtures\EventFixtures;
use App\Tests\DataFixtures\UserFixtures;
use App\Tests\Functional\Controller\EventControllerTest;
use GuzzleHttp\Utils;
use Symfony\Component\HttpFoundation\Request;

final class UpdateEventTest extends EventControllerTest
{
    /**
     * @dataProvider updateEventDataProvider
     */
    public function testUpdateEvent(string $eventUuid, array $testUserData, string $userEmail, array $responseBody): void
    {
        $jwt = $this->getJwt($userEmail);
        $this->request("/api/event/$eventUuid", Request::METHOD_PUT, $testUserData, ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        $controllerResponse = Utils::jsonDecode($this->client->getResponse()->getContent(), true);
        self::assertSame($responseBody, $controllerResponse);
    }

    public static function updateEventDataProvider(): array
    {
        return [
            'normal case' => [
                EventFixtures::EVENT_1_UUID,
                [
                    'name' => 'Not a very important event',
                    'coordinate' => [
                        'latitude' => 10.999999,
                        'longitude' => -120.5664,
                    ],
                ],
                EventFixtures::EVENT_TEST_USER_EMAIL,
                ['message' => [
                    'uuid' => EventFixtures::EVENT_1_UUID,
                    'name' => 'Not a very important event',
                    'venue' => 'Earth',
                    'coordinate' => [
                        'latitude' => 10.999999,
                        'longitude' => -120.5664,
                    ],
                    'startDate' => '2020-12-01 12:00:00',
                    'endDate' => '2020-12-01 13:00:00',
                ]],
            ],
            'lightweight case' => [
                EventFixtures::EVENT_2_UUID,
                [
                    'name' => 'Now we have some name',
                ],
                EventFixtures::EVENT_TEST_USER_EMAIL,
                ['message' => [
                    'uuid' => EventFixtures::EVENT_2_UUID,
                    'name' => 'Now we have some name',
                    'venue' => null,
                    'coordinate' => null,
                    'startDate' => null,
                    'endDate' => null,
                ]],
            ],
            'incorrect coordinate case' => [
                EventFixtures::EVENT_2_UUID,
                [
                    'coordinate' => [
                        'latitude' => 89.123456,
                        'longitude' => 181.2,
                    ],
                ],
                EventFixtures::EVENT_TEST_USER_EMAIL,
                ['error' => 'Invalid property coordinate.longitude: Must have a maximum value of 180'],
            ],
            'someone\'s event' => [
                EventFixtures::EVENT_1_UUID,
                [
                    'name' => 'Now we have some name',
                ],
                UserFixtures::STORED_TEST_USER_EMAIL,
                ['error' => 'User has no rights to edit the event'],
            ],
        ];
    }


}