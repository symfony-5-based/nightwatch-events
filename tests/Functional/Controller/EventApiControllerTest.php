<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Entity\Event;
use App\Tests\Functional\BaseFunctionalTest;
use GuzzleHttp\Utils;
use Symfony\Bridge\PhpUnit\ClockMock;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class EventApiControllerTest extends BaseFunctionalTest
{
    public function testEndToEndAuth(): void
    {
        ClockMock::register(Event::class);
        ClockMock::withClockMock(date_create('2020-12-12T20:30:00+00:00')->getTimestamp());

        $testUserData = ['email' => 'test00@example.com', 'password' => '1232153'];

        $this->request('/auth/register', Request::METHOD_POST, $testUserData);
        self::assertSame(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());

        $this->request('/auth/login', Request::METHOD_POST, $testUserData);
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $jwt = Utils::jsonDecode($this->client->getResponse()->getContent())->token;

        $this->request(
            '/api/event',
            Request::METHOD_POST,
            [
                'name' => 'Some fancy event name',
                'venue' => 'Assume it is a string name of a venue',
                'coordinate' => [
                    'latitude' => 89.123456,
                    'longitude' => 179.2,
                ],
                'startDate' => '2020-12-12T20:20:39+00:00',
                'endDate' => '2020-12-12T20:40:39+00:00'
            ],
            ['HTTP_Authorization' => 'Bearer ' . $jwt]
        );
        self::assertSame(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());
        $uuid = Utils::jsonDecode($this->client->getResponse()->getContent())->message;

        $this->request("/api/event/$uuid", Request::METHOD_GET, [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->request("/api/event/$uuid", Request::METHOD_DELETE, [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        self::assertSame(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());

        $this->request(
            "/api/event/$uuid",
            Request::METHOD_PUT,
            [
                'startDate' => '2020-12-12T20:35:39+00:00',
            ],
            ['HTTP_Authorization' => 'Bearer ' . $jwt]
        );
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->request("/api/event/$uuid", Request::METHOD_DELETE, [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->request("/api/event/$uuid", Request::METHOD_GET, [], ['HTTP_Authorization' => 'Bearer ' . $jwt]);
        self::assertSame(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
    }
}