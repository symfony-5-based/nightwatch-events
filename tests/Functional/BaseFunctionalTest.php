<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use GuzzleHttp\Utils;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseFunctionalTest extends WebTestCase
{
    protected ?KernelBrowser $client;

    public function setUp(): void
    {
        self::ensureKernelShutdown();
        $this->client = static::createClient();
    }

    public function tearDown(): void
    {
        $this->client = null;
    }

    protected function request(
        string $url,
        string $method = Request::METHOD_GET,
        array $requestBody = [],
        array $headers = [],
        array $parameters = [],
        array $files = []
    ): Crawler
    {
        return $this->client->request(
            $method,
            $url,
            $parameters,
            $files,
            $headers,
            Utils::jsonEncode($requestBody, JSON_FORCE_OBJECT)
        );
    }
}