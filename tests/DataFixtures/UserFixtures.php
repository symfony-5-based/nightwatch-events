<?php

declare(strict_types=1);

namespace App\Tests\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UserFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;

    public const STORED_TEST_USER_EMAIL = 'fixtest@example.com';
    public const STORED_TEST_USER_PASS = 'lalalala';

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail(self::STORED_TEST_USER_EMAIL);
        $user->setPassword($this->encoder->encodePassword($user, self::STORED_TEST_USER_PASS));

        $manager->persist($user);
        $manager->flush();
    }
}
