<?php

declare(strict_types=1);

namespace App\Tests\DataFixtures;

use App\Entity\Coordinate;
use App\Entity\Event;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class EventFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;

    public const EVENT_TEST_USER_EMAIL = 'j.difool@example.com';
    public const EVENT_TEST_USER_PASS = 'deepo';
    public const EVENT_1_UUID = '8dc3d2f0-6861-4e6b-b7d0-0a008db4fbd4';
    public const EVENT_2_UUID = 'c8592cc5-d423-47f4-81f3-b4f592604699';
    public const EVENT_3_UUID = '9c8f4290-5623-4746-86e9-800351a85ffc';
    public const EVENT_4_UUID = 'a04547c4-8a2f-4373-99de-adfaeb2fbd99';

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail(self::EVENT_TEST_USER_EMAIL);
        $user->setPassword($this->encoder->encodePassword($user, self::EVENT_TEST_USER_PASS));
        $manager->persist($user);

        $event1 = Event::create(
            $user,
            'Very important event',
            'Earth',
            Coordinate::create(10.0001, -120.5664),
            new DateTimeImmutable('2020-12-01T12:00:00+00:00'),
            new DateTimeImmutable('2020-12-01T13:00:00+00:00')
        );
        $event1->setUuid(UUID::fromString(self::EVENT_1_UUID));

        $event2 = Event::create($user);
        $event2->setUuid(UUID::fromString(self::EVENT_2_UUID));

        $event3 = Event::create(
            $user,
            'Another important event',
            'Earth',
            Coordinate::create(10.6513, -100.5664),
            new DateTimeImmutable('2020-12-01T14:00:00+00:00'),
            new DateTimeImmutable('2020-12-01T16:00:00+00:00')
        );
        $event3->setUuid(UUID::fromString(self::EVENT_3_UUID));

        $event4 = Event::create(
            $user,
            'Another important event',
            'Earth',
            Coordinate::create(10.6513, -100.5664),
            new DateTimeImmutable('2020-12-01T14:00:00+00:00'),
            new DateTimeImmutable('2020-12-01T16:00:00+00:00')
        );
        $event4->setUuid(UUID::fromString(self::EVENT_4_UUID));

        $manager->persist($event1);
        $manager->persist($event2);
        $manager->persist($event3);
        $manager->persist($event4);
        $manager->flush();
    }
}
