# Nigthwatch Events
Is an API for an Event Management System, which allows a client to perform event-related tasks.

# Installation
for the first installation do:

`git clone git@gitlab.com:symfony-5-based/nightwatch-events.git`

then, in a project run:

`composer install`

`php bin/console doctrine:schema:create`

you need to provide JWT_SECRET env variable
it is set by default for convenience
you can change it and store in `.env.local` file, or somewhere outside 

# Usage

to start the server run:

`symfony server:start`

then you can do requests (examples in [httpie](https://httpie.org/) style):

- **registration:** 

`http POST http://127.0.0.1:8000/auth/register \
 email=test@example.com password=passwrd`

- **login:**

`http POST http://127.0.0.1:8000/auth/login \
email=test@example.com password=passwrd`

responds with jwt key for the user, the jwt key must be used in headers of next requests
assume that our jwt is 
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.\
eyJ1c2VyIjoidGVzdEBleGFtcGxlLmNvbSIsImV4cCI6MTYwNjY3NDk1N30.ZaD7PCgnHnorDHqOuSBdEvmJnkNGUMPfSFvNhOdLXns`

- **create an event**

`http POST http://127.0.0.1:8000/api/event \
Authorization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.\
eyJ1c2VyIjoidGVzdEBleGFtcGxlLmNvbSIsImV4cCI6MTYwNjY3NDk1N30.\
ZaD7PCgnHnorDHqOuSBdEvmJnkNGUMPfSFvNhOdLXns" \
name="Nice Event" startDate="2020-11-29T00:00:00+00:00"`

- **update an event**

`http PUT http://127.0.0.1:8000/api/event/13fe95f9-56c5-4fbd-a7a8-f06bc63c98f5 \
Authorization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.\
eyJ1c2VyIjoidGVzdEBleGFtcGxlLmNvbSIsImV4cCI6MTYwNjY3NDk1N30.\
ZaD7PCgnHnorDHqOuSBdEvmJnkNGUMPfSFvNhOdLXns" \
name="A Very Nice Event" startDate="2020-11-29T00:00:00+00:00"`

- **get an event**

`http GET http://127.0.0.1:8000/api/event/13fe95f9-56c5-4fbd-a7a8-f06bc63c98f5 \
Authorization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.\
eyJ1c2VyIjoidGVzdEBleGFtcGxlLmNvbSIsImV4cCI6MTYwNjY3NDk1N30.\
ZaD7PCgnHnorDHqOuSBdEvmJnkNGUMPfSFvNhOdLXns"`

- **delete an event**

`http DELETE http://127.0.0.1:8000/api/event/13fe95f9-56c5-4fbd-a7a8-f06bc63c98f5 \
Authorization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.\
eyJ1c2VyIjoidGVzdEBleGFtcGxlLmNvbSIsImV4cCI6MTYwNjY3NDk1N30.\
ZaD7PCgnHnorDHqOuSBdEvmJnkNGUMPfSFvNhOdLXns"`

- **get a list of events by a field**

`http POST http://127.0.0.1:8000/api/eventList \
Authorization:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.\
eyJ1c2VyIjoidGVzdEBleGFtcGxlLmNvbSIsImV4cCI6MTYwNjY3NDk1N30.\
ZaD7PCgnHnorDHqOuSBdEvmJnkNGUMPfSFvNhOdLXns" \
name="A Very Nice Event"`

currently only full match by fields "name", "venue", "coordinate"

## Comments
Because of generic nature of the app, some design decisions could be questionable, here are several comments about the design 
- venue is an abstract entity, is hard to say, what should it be, it can be a predefined entity, and another application is responsible for providing its data, it can be just a name of some place, that person gave it, also it could have its own coordinates, if event coordinates are set or not;
to avoid misunderstanding, it was made abstract nullable string from an event
- it is hard to say what event's fields can be optional, I wouldn't like to add restrictions without additional details,
so I did all as optional ones (even event time logic is not enough to determine it)
- real life example of getting list of events by queries would take a lot, here is used a very simple one

## Database diagram
![diagram](./database_diagram.jpg?raw=true) 

## License
[MIT](https://choosealicense.com/licenses/mit/)