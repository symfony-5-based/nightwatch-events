<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Firebase\JWT\JWT;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

final class JwtAuthenticator extends AbstractGuardAuthenticator
{
    private EntityManagerInterface $em;
    private ContainerBagInterface $params;

    public function __construct(EntityManagerInterface $em, ContainerBagInterface $params)
    {
        $this->em = $em;
        $this->params = $params;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(
            [
                'message' => 'Authentication Required',
            ],
            Response::HTTP_UNAUTHORIZED
        );
    }

    public function supports(Request $request)
    {
        return $request->headers->has('Authorization');
    }

    public function getCredentials(Request $request)
    {
        return $request->headers->get('Authorization');
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = $this->getUserByCredentials($credentials);
        if ($user === null) {
            throw new AuthenticationException('Authentication Required');
        }
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // checked in getUser
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse(
            [
                'message' => $exception->getMessage(),
            ],
            Response::HTTP_UNAUTHORIZED
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
    }

    public function supportsRememberMe()
    {
        return false;
    }

    public function getUserByRequest(Request $request): ?User
    {
        return $this->getUserByCredentials($this->getCredentials($request));
    }

    private function getUserByCredentials($credentials): ?User {
        try {
            $jwt = (array)JWT::decode(
                str_replace('Bearer ', '', $credentials),
                $this->params->get('jwt_secret'),
                [$this->params->get('jwt_algo')]
            );
            /** @var User|null $user */
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $jwt['user']]);
            return $user;
        } catch (Exception $exception) {
            throw new AuthenticationException($exception->getMessage());
        }
    }
}