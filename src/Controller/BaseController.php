<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

abstract class BaseController extends AbstractController
{
    protected function error(string $msg): JsonResponse
    {
        return $this->json(['error' => $msg], HttpResponse::HTTP_BAD_REQUEST);
    }

    protected function ok($msg, int $httpResponseCode = HttpResponse::HTTP_OK): JsonResponse
    {
        return $this->json(['message' => $msg], $httpResponseCode);
    }
}