<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Event;
use App\Entity\User;
use App\JsonSchema\ValidationException;
use App\JsonSchema\Validator;
use App\Repository\EventRepository;
use App\Security\JwtAuthenticator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

final class EventController extends BaseController
{
    /**
     * @throws ORMException
     * @throws Exception
     */
    public function create(Request $request, JwtAuthenticator $authenticator, EventRepository $eventRepository): JsonResponse
    {
        try {
            $eventData = $this->getValidatedEventData($request);
        } catch (ValidationException $e) {
            return $this->error($e->getMessage());
        }

        /** strictly - user could be null, but in this case we already denied the request via security module */
        $event = $eventRepository->createEvent($authenticator->getUserByRequest($request), $eventData);

        return $this->ok($event->getUuid(), HttpResponse::HTTP_CREATED);
    }


    public function getEvent(string $eventUuid, EventRepository $eventRepository): JsonResponse
    {
        $event = $eventRepository->getByUuid($eventUuid);
        return $event !== null
            ? $this->ok($event->toArray())
            : $this->error("Event $eventUuid was not found");
    }


    /**
     * @throws Exception
     */
    public function updateEvent(string $eventUuid, Request $request, JwtAuthenticator $authenticator, EventRepository $eventRepository): JsonResponse
    {
        $event = $eventRepository->getByUuid($eventUuid);
        if ($event === null) {
            return $this->error("Event $eventUuid was not found");
        }

        /** @var User $user */
        $user = $authenticator->getUserByRequest($request);
        if ($event->getUser()->getId() !== $user->getId()) {
            return $this->error("User has no rights to edit the event");
        }

        try {
            $eventData = $this->getValidatedEventData($request);
        } catch (ValidationException $e) {
            return $this->error($e->getMessage());
        }

        $eventRepository->updateEventWithNewData($event, $eventData);
        return $this->ok($event->toArray());
    }


    /**
     * @throws OptimisticLockException|ORMException
     */
    public function deleteEvent(string $eventUuid, Request $request, JwtAuthenticator $authenticator, EventRepository $eventRepository): JsonResponse
    {
        $event = $eventRepository->getByUuid($eventUuid);
        if ($event === null) {
            return $this->error("Event $eventUuid was not found");
        }

        /** @var User $user */
        $user = $authenticator->getUserByRequest($request);
        if ($event->getUser()->getId() !== $user->getId()) {
            return $this->error("User has no rights to delete the event");
        }

        if (!$event->canBeDeleted()) {
            return $this->error('Ongoing event cannot be deleted');
        }

        $eventRepository->removeEvent($event);
        return $this->ok('Event was deleted');
    }


    public function getEventList(Request $request, EventRepository $eventRepository): JsonResponse
    {
        try {
            $eventQueryData = $this->getValidatedEventData($request);
        } catch (ValidationException $e) {
            return $this->error($e->getMessage());
        }

        $eventList = $eventRepository->findByQuery($eventQueryData);
        return $this->json(
            [
                'message' => 'ok',
                'events' => array_map(static fn (Event $event): array => $event->toArray(), $eventList)
            ]
        );
    }


    /**
     * @throws ValidationException
     */
    private function getValidatedEventData(Request $request): object
    {
        return Validator::getValidatedObject($request->getContent(), 'event');
    }
}
