<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\EventRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $uuid;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $venue;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     */
    private User $user;

    /**
     * @ORM\ManyToOne(targetEntity=Coordinate::class, cascade={"persist"})
     */
    private ?Coordinate $coordinate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $endDate;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function setUuid($uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getVenue(): ?string
    {
        return $this->venue;
    }

    public function setVenue(?string $venue): self
    {
        $this->venue = $venue;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCoordinate(): ?Coordinate
    {
        return $this->coordinate;
    }

    public function setCoordinate(?Coordinate $coordinate): self
    {
        $this->coordinate = $coordinate;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public static function create(
        User $user,
        string $name = null,
        string $venue = null,
        Coordinate $coordinate = null,
        DateTimeInterface $startDate = null,
        DateTimeInterface $endDate = null
    ): self {
        $event = new self();
        $event->uuid = Uuid::uuid4();
        $event->user = $user;
        $event->name = $name;
        $event->coordinate = $coordinate;
        $event->venue = $venue;
        $event->startDate = $startDate;
        $event->endDate = $endDate;
        return $event;
    }

    public function toArray(): array
    {
        $coordinate = $this->getCoordinate();
        $startDate = $this->getStartDate();
        $endDate = $this->getEndDate();

        return [
            'uuid' => $this->getUuid(),
            'name' => $this->getName(),
            'venue' => $this->getVenue(),
            'coordinate' => $coordinate ? $coordinate->toArray() : null,
            'startDate' => $startDate ? $startDate->format('Y-m-d H:i:s') : null,
            'endDate' => $endDate ? $endDate->format('Y-m-d H:i:s') : null,
        ];
    }

    public function canBeDeleted(): bool
    {
        $startDate = $this->getStartDate();
        $endDate = $this->getEndDate();
        $now = (new \DateTime)->setTimestamp(time());
        return $startDate === null
            || ($endDate !== null && $endDate <= $now)
            || $startDate > $now;
    }
}
