<?php

declare(strict_types=1);

namespace App\JsonSchema;

use GuzzleHttp\Exception\InvalidArgumentException;
use GuzzleHttp\Utils;
use stdClass;

abstract class Validator
{
    /**
     * @throws ValidationException
     */
    public static function getValidatedObject(string $content, string $schemaFileName): object
    {
        try {
            $contentData = $content === '{}' ? new stdClass() : Utils::jsonDecode($content);
        } catch (InvalidArgumentException $e) {
            throw new ValidationException("Incorrect body");
        }

        $validator = new \JsonSchema\Validator;
        $validator->validate($contentData, (object)['$ref' => 'file://' . __DIR__ . DIRECTORY_SEPARATOR . $schemaFileName . '.json']);
        if ($validator->isValid()) {
            return $contentData;
        }

        throw new ValidationException(
            implode(
                ',',
                array_map(
                    static fn (array $error): string => "Invalid property {$error['property']}: {$error['message']}",
                    $validator->getErrors()
                )
            )
        );
    }
}