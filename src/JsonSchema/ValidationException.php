<?php

declare(strict_types=1);

namespace App\JsonSchema;

use Exception;

final class ValidationException extends Exception
{
}