<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Coordinate;
use App\Entity\Event;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    public function getByUuid(string $uuid): ?Event
    {
        return $this->findOneBy(['uuid' => $uuid]);
    }

    /**
     * @throws ORMException
     */
    public function storeEvent(Event $event): void
    {
        $em = $this->_em;
        $em->persist($event);
        $em->flush();
    }

    /**
     * @throws OptimisticLockException|ORMException
     */
    public function removeEvent(Event $event): void
    {
        $em = $this->_em;
        $em->remove($event);
        $em->flush();
    }

    public function findByQuery(object $queryObject): array
    {
        /** Condition duplication to prevent do any queries in case in is a search without filters */
        if (!isset($queryObject->name) && !isset($queryObject->venue) && !isset($queryObject->coordinate)) {
            return [];
        }

        $builder = $this->createQueryBuilder('event');
        if (isset($queryObject->name)) {
            $builder->andWhere('event.name = :name')->setParameter('name', $queryObject->name);
        }
        if (isset($queryObject->venue)) {
            $builder->andWhere('event.venue = :venue')->setParameter('venue', $queryObject->venue);
        }
        if (isset($queryObject->coordinate)) {
            $builder->leftJoin('event.coordinate', 'event_coordinate')
                ->andWhere('event_coordinate.latitude = :latitude')->setParameter('latitude', $queryObject->coordinate->latitude)
                ->andWhere('event_coordinate.longitude = :longitude')->setParameter('longitude', $queryObject->coordinate->longitude);
        }
        return $builder->setMaxResults(10)->getQuery()->execute();
    }

    /**
     * @throws ORMException
     * @throws Exception
     */
    public function createEvent(User $user, object $validatedEventData): Event
    {
        $event = Event::create(
            $user,
            $validatedEventData->name ?? null,
            $validatedEventData->venue ?? null,
            !isset($validatedEventData->coordinate->latitude, $validatedEventData->coordinate->longitudeCoordinate)
                ? null
                : Coordinate::create($validatedEventData->coordinate->latitude, $validatedEventData->coordinate->longitude),
            isset($validatedEventData->startDate) ? new DateTimeImmutable($validatedEventData->startDate) : null,
            isset($validatedEventData->endDate) ? new DateTimeImmutable($validatedEventData->endDate) : null
        );
        $this->storeEvent($event);
        return $event;
    }

    /**
     * @throws Exception
     */
    public function updateEventWithNewData(Event $event, object $validatedEventData): void
    {
        if (isset($validatedEventData->name)) {
            $event->setName($validatedEventData->name);
        }
        if (isset($validatedEventData->venue)) {
            $event->setName($validatedEventData->venue);
        }
        if (isset($validatedEventData->coordinate)) {
            $event->setCoordinate(Coordinate::create($validatedEventData->coordinate->latitude, $validatedEventData->coordinate->longitude));
        }
        if (isset($validatedEventData->startDate)) {
            $event->setStartDate(new DateTimeImmutable($validatedEventData->startDate));
        }
        if (isset($validatedEventData->endDate)) {
            $event->setStartDate(new DateTimeImmutable($validatedEventData->endDate));
        }
        $this->storeEvent($event);
    }
}
